﻿using System;

namespace IMEIGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            string start = "939";
            string end = "5";
            string imei = start;

            Random rand = new Random();

            for(int i = 0; i < 11; i++)
            {
                imei += ""+rand.Next(0,9);
            }

            imei = imei + end;

            Console.WriteLine("IMEI generated is: {0}", imei);
        }
    }
}
